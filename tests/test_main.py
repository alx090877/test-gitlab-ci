from fastapi.testclient import TestClient

from app.main import app


client = TestClient(app)


def test_create_recipe():
    recipe = {
        "title": "Test Recipe",
        "cooking_time": 69,
        "ingredients": "potatoes, carrots, beets",
    }
    response = client.post("/cookbook/add", json=recipe)
    assert response.status_code == 200
    assert response.json()["title"] == "Test Recipe"


def test_get_all_recipes():
    response = client.get("/cookbook/")
    assert response.status_code == 200
    assert len(response.json()) > 0


def test_get_recipe_by_id():
    recipe = {"title": "Test Recipe 2", "cooking_time": 40, "ingredients": "potatoes"}
    response = client.post("/cookbook/add", json=recipe)
    recipe_id = response.json()["id"]
    response = client.get(f"/cookbook/{recipe_id}")
    assert response.status_code == 200
    assert response.json()["title"] == "Test Recipe 2"
