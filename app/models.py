from sqlalchemy import Column, String, Integer, Float, Text

from database import Base


class CookBook(Base):
    __tablename__ = "cookbook"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    cooking_time = Column(Float, nullable=False)
    description = Column(Text)
    views = Column(Integer, default=0)
    ingredients = Column(Text)
