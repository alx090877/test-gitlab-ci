from pydantic import BaseModel
from typing import Optional


class BaseBook(BaseModel):
    title: str
    cooking_time: float


class CookBookMain(BaseBook):
    id: int

    class Config:
        orm_mode = True


class CookBookIn(BaseBook):
    description: Optional[str] = None
    ingredients: str


class CookBookDetails(CookBookIn):
    id: int
    views: int

    class Config:
        orm_mode = True
