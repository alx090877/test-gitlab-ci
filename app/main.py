from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

import models
import schemas
from database import engine, get_async_session

app = FastAPI()


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()


@app.get("/cookbook/", response_model=List[schemas.CookBookMain])
async def cookbook_main(
    session: AsyncSession = Depends(get_async_session),
) -> List[models.CookBook]:
    result = await session.execute(
        select(models.CookBook).order_by(models.CookBook.views.desc())
    )
    return result.scalars().all()


@app.get("/cookbook/{idx}", response_model=schemas.CookBookDetails)
async def cookbook_detail(
    idx: int, session: AsyncSession = Depends(get_async_session)
) -> models.CookBook:
    result = await session.execute(select(models.CookBook).filter_by(id=idx))
    result = result.scalars().one_or_none()

    if not result:
        raise HTTPException(status_code=404, detail=f"Рецепт  с id:{idx} не найден")

    result.views += 1
    await session.commit()

    return result


@app.post("/cookbook/add", response_model=schemas.CookBookDetails)
async def add_recipe(
    recipe: schemas.CookBookIn, session: AsyncSession = Depends(get_async_session)
) -> models.CookBook:
    new_recipe = models.CookBook(**recipe.dict())
    print(new_recipe)
    async with session.begin():
        session.add(new_recipe)
    return new_recipe
